import { Triple } from "./triple"
import { AsyncIterableX } from "ix/asynciterable/asynciterablex"

export const UNDEF: unique symbol = Symbol("undef")

export function duplicateLast<V, E>(solution: Solution<V, E>): Solution<V, E> {
  const { collected, results } = solution
  return {
    collected,
    results: [...results, lastOrError(results)]
  }
}

export function sameValueZero(x: any, y: any): boolean {
  return x === y || Object.is(x, y)
}

export function unpackArg<T = any>(args: T[]): T | typeof UNDEF {
  if (args.length === 0) {
    return UNDEF
  }
  return args[0]
}

export function unpackArgs<T>(length: number, args: T[]): (T | typeof UNDEF)[] {
  const ret: any[] = args.slice()
  for (let i = ret.length; i < length; i++) {
    ret.push(UNDEF)
  }
  return ret
}

export interface Solution<V, E> {
  readonly results: ReadonlyArray<SolutionStep<V, E>>
  readonly collected: Record<string | number | symbol, unknown>
}

export interface SolutionStep<V, E> {
  readonly triple?: Triple<V, E>
  readonly value: V | E
}

export function from<T>(x: Iterable<T>): Iterable<T> {
  return x
}

// This function is named such that "from" can just be prepended with "async".
export function asyncfrom<T>(x: Iterable<T>): AsyncIterable<T> {
  return AsyncIterableX.from(x)
}

export function lastOrError<T>(arr: ReadonlyArray<T>): T {
  if (arr.length === 0) {
    throw new TypeError("array is empty")
  }
  return arr[arr.length - 1]
}
