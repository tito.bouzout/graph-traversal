import { Triple } from "./triple"

export interface Graph<V, E> {
  match(pattern: Partial<Triple<V, E>>): IterableIterator<Triple<V, E>>
}

export interface AsyncGraph<V, E> {
  match(pattern: Partial<Triple<V, E>>): AsyncIterableIterator<Triple<V, E>>
}
